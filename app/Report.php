<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Report extends Model
{
    public $guarded = ['id'];
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->crowded = self::cleanUp($model->crowded);
            $model->least = self::cleanUp($model->least);
        });

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('day', 'desc');
        });
    }



    public static function cleanUp($str)
    {
        return str_replace('sf', '', $str);
    }

    public static function getCurrentState()
    {
        $state = Report::first();
        if (!$state) {
            $state = (object) [
                'day' => 1,
                'sf1' => 0,
                'sf2' => 0,
                'sf3' => 0,
                'sf4' => 0,
            ];
        }
        return $state;
    }
}