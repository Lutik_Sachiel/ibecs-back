<?php

use Illuminate\Http\Request;
use App\Report;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/dailyReport', function (Request $request) {
    $report = Report::insert([
        'day' => $request->input('day'),
        'total' => $request->input('total'),
        'dead' => $request->input('dead'),
        'alive' => $request->input('alive'),
        'crowded' => $request->input('crowded'),
        'least' => $request->input('least'),
        'sf1' => $request->input('sf1'),
        'sf2' => $request->input('sf2'),
        'sf3' => $request->input('sf3'),
        'sf4' => $request->input('sf4'),
    ]);
    return response()->json($report);
});

Route::get('/getState', function () {
    $state = Report::getCurrentState();
    return response()->json($state);
});

Route::delete('/cleanState', function () {
   Report::truncate();
});

Route::get('/getReports', function () {
    $reports = Report::orderBy('day', 'asc')->get();
    return response()->json($reports);
});