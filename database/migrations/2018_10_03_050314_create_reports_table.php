<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('day');
            $table->integer('total')->comment('общее количество овечек ');
            $table->integer('dead')->comment('количество убитых овечек ');
            $table->integer('alive')->comment('количество живых овечек');
            $table->integer('crowded')->comment('номер самого населённого загона');
            $table->integer('least')->comment('номер самого менее населённого загона');
            $table->integer('sf1');
            $table->integer('sf2');
            $table->integer('sf3');
            $table->integer('sf4');
            $table->index('day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
